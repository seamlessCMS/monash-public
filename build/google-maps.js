$(document).ready(function () {
    var SEAMLESS = SEAMLESS || {};
    (function (googleMaps, $, undefined) {
 
        googleMaps.initialise = function() {
            var $mapContainers = $('.gmap'),
                $breakpointXS = $('.breakpoint-sc-size-4'),
                $breakpointS = $('.breakpoint-sc-size-3');
            
            if($breakpointXS.length || $breakpointS.length) {
                //don't load maps JS if mobile device
            } else {
                $($mapContainers).each(function() {
                    InitialiseMap(this);
                });
            }	
        };
        var markers = [];
 
        function InitialiseMap(mapContainer)
        {
            var $data = $(mapContainer).data();
            var $markerSourceElements = $(mapContainer).find('.gmap-marker');
            var targetDiv = $(mapContainer).find('.gmap-target')[0];
            $(targetDiv).find('img').remove();
            var map = {
                container : mapContainer,
                markers : [],
                markerSourceElements : $markerSourceElements,
                isMultiple : $markerSourceElements.size() > 1,
                params : $data['params'],
                targetDiv : targetDiv,
                googleMap : CreateGoogleMap(targetDiv, $data['params']),
                infoWindow : new google.maps.InfoWindow()
            };
            map.infoWindow.setOptions({maxWidth: 295});
            AddMarkers(map);
			
			var mcOptions = {
			  'zoom': 13
			};
			var mc = new MarkerClusterer(map.googleMap, markers, mcOptions);
            if(map.params.fitBounds === true)
            {
                SetBounds(map);
            }
			var mcOptions = {
			  'zoom': 13
			};
			var mc = new MarkerClusterer(map.googleMap, markers, mcOptions);

        }
 
        function CreateGoogleMap(mapContainer, data)
        {
            var centerPoint = GetCenterPoint(data);
            var mapOptions = {
                zoom : data.zoom,
                center : centerPoint,
                mapTypeId : google.maps.MapTypeId.ROADMAP
            };
            mapOptions.disableDefaultUI = data.hideControls === true;
            mapOptions.minZoom = (data.minZoom !== undefined) ? data.minZoom : 0;
            mapOptions.maxZoom = (data.maxZoom !== undefined) ? data.maxZoom : 21;
            return new google.maps.Map(mapContainer, mapOptions);
        }
 
        function GetCenterPoint(data)
        {
            if(data.centerPoint === undefined)
            {
                return new google.maps.LatLng(-37.818157, 144.960393);
            }
            var latLong = ConvertLatLongToFloat(data.centerPoint);
            if(latLong.length == 2)
            {
                return new google.maps.LatLng(latLong[0],latLong[1]);
            }
            return new google.maps.LatLng(-37.818157, 144.960393);
        }
 
        function AddMarkers(map)
        {
            $(map.markerSourceElements).each(function() {
                var latLongStr = $(this).find('.gmap-latlong').first().html();
                var address = $(this).find('.gmap-address').first().html();
                var info = $(this).find('.gmap-info').first().html();
                var latLong = ConvertLatLongToFloat(latLongStr);
                if($(this).find('.gmap-icon').length > 0)
                    var icon = ProcessIconLink($(this).find('.gmap-icon').first().html());
                else
                    var icon = null;
                var markerInfo = {
                    latLong : latLong,
                    address : address,
                    info : info
                };
                AddMarker(markerInfo, map, icon);
            });
        }
        
        function ProcessIconLink(link)
        {
            link = link.replace(/ &amp; /g, '_').replace(/ /g, '_').toLowerCase();
            
            var idx = link.lastIndexOf("/");
            var path = link.substr(0, idx + 1);
            var iconName = link.substr(idx + 1, link.length - idx - 1);
            
            if(iconName.indexOf("community") >= 0)
                iconName = "comm";
            else if(iconName.indexOf("event") >= 0)
                iconName = "upco";
            else if(iconName.indexOf("park") >= 0)
                iconName = "park";
            else if(iconName.indexOf("recreation") >= 0)
                iconName = "aqua";
            else if(iconName.indexOf("hall") >= 0)
                iconName = "halls";
            else if(iconName.indexOf("libra") >= 0)
                iconName = "libr";
            else if(iconName.indexOf("youth") >= 0 || iconName.indexOf("yfs") >= 0)
                iconName = "yout";
            else if(iconName.indexOf("maternal") >= 0 || iconName.indexOf("mch") >= 0)
                iconName = "mate";
            
            iconName = "flag_" + iconName + ".png"
            
            return path + iconName;
        }
 
        function ConvertLatLongToFloat(latLongStr)
        {
            var split = latLongStr.split(',');
            var latLong = [];
            if(split.length != 2)
            {
                return latLong;
            }
            latLong[0] = parseFloat(split[0]);
            latLong[1] = parseFloat(split[1]);
            return latLong;
        }
 
        function AddMarker(markerInfo, map, icon)
        {
            if(markerInfo.latLong.length == 2)
            {
                AddMarkerUsingLatLong(new google.maps.LatLng(
                    markerInfo.latLong[0], markerInfo.latLong[1]), markerInfo, map, icon);
            }
            else
            {
                AddMarkerUsingGeoCodedAddress(markerInfo, map, icon);
            }
        }
 
        function AddMarkerUsingLatLong(latLong, markerInfo, map, icon)
        {
            if(icon != null)            
                var marker = new google.maps.Marker({
                    position: latLong,
                    map: map.googleMap,
                    icon: icon
                });
            else
                var marker = new google.maps.Marker({
                    position: latLong,
                    map: map.googleMap
                });
            google.maps.event.addListener(marker, 'click', function()
            {
                map.infoWindow.setContent(markerInfo.info);
                map.infoWindow.open(map.googleMap, marker);
 
            });
            AddDomListenerToCenterOnAutoOpenedInfoWindow(markerInfo, map);
            markers.push(marker);
        }
 
        function AddDomListenerToCenterOnAutoOpenedInfoWindow(markerInfo, map)
        {
            if(map.params.autoOpenInfoWindow === true && map.isMultiple === false)
            {
                google.maps.event.addDomListener(window, "load", function()
                {
                    map.infoWindow.setContent(markerInfo.info);
                    if(markers.length==1)
                    {
                        map.infoWindow.open(map.googleMap,markers[0]);
                    }
                });
            }
        }
 
        function AddMarkerUsingGeoCodedAddress(markerInfo, map, icon)
        {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { address : markerInfo.address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    var latLong = results[0].geometry.location;
                    AddMarkerUsingLatLong(latLong, markerInfo, map, icon);
                }
                else
                {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
 
        function SetBounds(map)
        {
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < markers.length; i++)
            {
                var position = markers[i].position;
                bounds.extend(position);
            }
            map.googleMap.fitBounds(bounds);
        }
 
    }(SEAMLESS.googleMaps = SEAMLESS.googleMaps || {}, jQuery));
    SEAMLESS.googleMaps.initialise();

});