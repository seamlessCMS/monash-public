var monash_site = {
    init:function() {    
        this.pageControlInsert();
        this.successHighlight();

        this.checkUserLoggedIn();
        
        this.addValueAll();
        this.addAccordion();
        this.addFancyBox(); //pop up gallery
        this.addToggles();
        this.addMobileToggles();
        this.addMobilePanelToggles();
        this.addMobilePhonePanelToggles();
        this.addMenuHover();
        this.addTabs();
        this.addStickySideBar();
        
        this.tidyUpForms(); //generic call to fix/style/format form elements
        this.tidyUpSearchResults();
        this.tidyUpResizeControls();
        this.tidyUpSynonym();
        this.tidyUpTabs();
        this.tidyUpMyCouncil();
        this.tidyUpLanding();
        //this.setHomeEventsLinkHeight();
        this.tidyUpHome();
        this.tidyUpTables();
        this.tidyUpTickIcons();
        this.tidyUpFeaturedNews();
        
        this.toggleAddLinkForm();
        this.toggleHelp(); //qTip
        this.toggleMobileMainNav();
        this.toggleListViews(); //switch between list and map views

        this.fadeOutSuccess();
        this.resizeHandler();

        this.echoVersionNumber();
    },
    echoVersionNumber: function() {
        var version = "version 0.15b";
        console.log(version);
    },
    pageControlInsert: function() {
        var printMarker = $('.sharing-container ul li.first');
        if(printMarker.length) {
            printMarker.append($('<a href="#" title="Print"><i class="icon-print"></i><span class="icon-description">Print</span></a>'));
        }
        $('.sharing-container ul li.first').on("click", "a", function(event) {
            window.print();
            event.preventDefault();
        });
    },
    successHighlight:function() {
        var success = $('.success');
        if(success.length) {
            success.show().delay(1000).removeClass('success');
        }
    },
    checkUserLoggedIn:function() {
        var $userLoggedInChecker = $('body').attr('data-username'),
            $userClass = $('.userLogin');
        if(($userLoggedInChecker !== '') && ($userClass.length)) {
            $userClass.addClass('userLogin--active');
        }
    },
    addValueAll:function() {
        if($('#event-type-container select').length) {
            $('#event-type-container option:first').text("All");
        }
    },
    addAccordion:function() {
        var $accordionDetect = $('#accordion');
        if($accordionDetect.length) {
            $accordionDetect.accordion({
                heightStyle: "content",
                collapsible: true

            });
        }
    },
    addFancyBox:function() {
        function formatTitle(title, currentArray, currentIndex, currentOpts) {
            return '<p id="tip-title">' + (title && title.length ? '<b>' + title + '</b>' : '' ) + '<i>Photo ' + (currentIndex + 1) + ' of ' + currentArray.length + '</i></p>';
        }

        if($('.photoGallery_item').length) {
            var $galleryFirstChild =  $('.photoGallery li:first-child');
            //add open button
            $galleryFirstChild.children('a').append('<p class="photoGallery_open"><i class="icon-fullscreen"></i> Click to view gallery</p>');
            
            //show first item //rest are closed via CSS
            $galleryFirstChild.addClass('photoGallery_item--active');

            //close button
            $('.fancyboxClose').click(function() {
                $.fancybox.close();
            });

            //do fancybox magix
            $('.photoGallery_item a').fancybox({
                // openEffect  : 'none',
                // closeEffect : 'none',
                padding : 25,
                prevEffect : 'none',
                nextEffect : 'none',
                cyclic : true,

                // closeBtn  : false,
                'titlePosition'     : 'inside',
                'titleFormat'       : formatTitle,
                'onComplete'    :   function() {
                    //update close button
                    if($('.fancyboxClose').length < 1) {
                        $('#fancybox-close').append('<span class="fancyboxClose">Close <i class="icon-close"></i></span>'); 
                    }
                }
            });
        }
    },

    addMenuHover:function() {
        var $menuItems = $('.mainMenu_list > li a'),
            $menuLiItems = $('.mainMenu_list > li');

        /* hover */
        $menuLiItems.hoverIntent({
            over: monash_site.hiOver,
            out: monash_site.hiOut,
            interval: 200
        });

        /* focus */
        $menuItems.focusin(function(){ 
            $(this).parents('li.nav-has-children').addClass('hiHover');
        });
        $menuItems.focusout(function(){
            $menuItems.parents('li.nav-has-children').removeClass('hiHover');
        });
       
        $menuItems.keydown(function(e){
            $this = $(this);
            if($this.is(':focus')) {
                if(e.keyCode == 38 || e.keyCode == 37) { //KEY_UP_ARROW or LEFT
                    monash_site.simulateTab(this, 'prev');
                    e.preventDefault();
                }
                else if(e.keyCode == 40 || e.keyCode == 39) { //KEY_DOWN_ARROW OR RIGHT
                    monash_site.simulateTab(this, 'next');
                    e.preventDefault();
                } 
            }
        });
        /* touch */
        if($('.touch').length) {
            $('.mainMenu_list > li').on({
                'touchstart' : function(e){
                    e.stopPropagation();
                    //"use strict"; //satisfy the code inspectors
                    var menuItem = $(this);
                    if (menuItem.hasClass('nav-has-children')) {
                        if(menuItem.hasClass('hiHover')) {
                            return true;
                        } else {
                            e.preventDefault();
                            menuItem.addClass('hiHover');
                            $menuLiItems.not(this).removeClass('hiHover');
                        }
                    }
                }
            });
            if($('.hiHover').length) {
                $('body').on({ //this has to be a bad idea
                    'touchstart' : function(){
                        $menuLiItems.removeClass('hiHover');
                        //console.log('catch all screwing things up');
                        return true; 
                    }
                });
            }           
        }
    },
    hiOver:function() {
        $(this).addClass('hiHover');
    },
    hiOut:function() {
        $(this).removeClass('hiHover');
    },
    simulateTab:function(currentElement, direction){
        var $menuItems = $('.mainMenu_list > li a');

        for (var j = 0, jl = $menuItems.length; j < jl; j++) {
            if (currentElement === $menuItems[j]) {
                if(direction == 'next') {
                    if (j+1 < jl) {
                        $($menuItems[j+1]).focus();
                        $($menuItems[j+1]).trigger('focusin');
                    } else {
                        $(currentElement).blur();
                    }
                } else {
                    if (j-1 >= 0) {
                        $($menuItems[j-1]).focus();
                        $($menuItems[j+1]).trigger('focusin');
                    } else {
                        $(currentElement).blur();
                    }
                }
            }
        }
    },

    addTabs: function() {
        if($('.tabArea_item').length){
            
            if(monash_site.isMobilePhone()) { // if a mobile device toggle
                //reset if resized

            } else { // else tab
                //reset if resized

                //hide all tabList_item except the first
                $(".tabArea_item:gt(0)").addClass('hidden-tabs');

                //create the tabs
                var $tabList = $('<ul/>').addClass('tabList'),
                    $tabbedArea = $('.tabbedArea');

                $.each($('.tabArea_item'), function(i){
                    var li = $('<li/>')
                        .addClass('tabList_item')
                        .appendTo($tabList);    
                    var aaa = $('<a/>')
                        .attr('href', '#'+ $(this).attr('id'))
                        .addClass('tabList_link')
                        .text($(this).attr('data-tab'))
                        .appendTo(li);    
                });
                $tabbedArea.prepend($tabList);
                //final set up for plugin
                $('.tabList a:first').addClass('current');
                //letz tab
                $tabbedArea.organicTabs();
            }
        }
    },
    addStickySideBar:function() {
        
        if($('.touch').length) {
            //do nothing 
            // - the iPad momentum scrolling breaks sticky sidebar
        } else {
            monash_site.showStickySideBar();
        }
    },
    showStickySideBar:function() {
        var $sidebar = $('#sidebar');

        if($sidebar.length) {
            $('#sidebar').stickySidebar();
        }
    },
    tidyUpTables: function() {
        if($('.rd-dates th').length >= 7) {
            var widthCell = $('.rd-dates th').length * 116;
            $('.rd-dates table').css('width', widthCell + 'px');
        }

        monash_site.tidyUpTableMinutesMobile();
    },
    tidyUpTableMinutesMobile:function() {
        if(monash_site.isMobilePhone()) {
            if($('.minutes-table').length && $('.meeting-date').length === 0){ 
                var $meetingDate = $('<table/>').addClass('table-styled meeting-date'),
                    $minutesTable = $('table.minutes-table'),
                    $minutesTableRow = $('.minutes-table tr'),
                    $tbody = $meetingDate.appendTo($('<tbody/>'));

                    $minutesTable.wrap( "<div class='tableWrapper'></div>" );

                $.each($('.minutes-table tbody tr'), function(i){
                    var $createRow = $('<tr/>')
                        .appendTo($tbody);
                    var $firstChild = $(this).find(">:first-child")
                        .appendTo($createRow);
                });
                var $thead = $('<thead/>').prependTo($meetingDate),
                    $theadRow = $('<tr/>').appendTo($thead);

                $('.minutes-table thead tr').find(">:first-child").appendTo($theadRow);

                $minutesTable.before($meetingDate);
                $minutesTable.wrap( "<div class='tableWrapper--scroll'></div>" );
                $minutesTable.wrap( "<div class='tableWrapper--scrollDiv'></div>" );//scroll div
                $('.meeting-date').wrap( "<div class='tableWrapper--fixed'></div>" );
            } 
        }
    },
    tidyUpForms:function() {
        monash_site.tidyUpSelectDropdowns();
        monash_site.safariMobileSelectFix();
        monash_site.removeEmptyErrorUL();
        monash_site.tidyUpLabels();
        monash_site.passSearchValues();
        monash_site.moveURLLabel();
        monash_site.moveTermsLabel();

        //convert multi selects
        $('.seForm select[multiple="multiple"]').chosen({
            width: "100%"
        });
    },
    passSearchValues: function() {
        //"dear god why?!" you ask? 
        // - to pass value on tablet orientation change
        var $mobileSearchInput = $('.mobileMenu input.ps-input'),
            $desktopSearchInput = $('.mainMenu input.ps-input');

        if(($mobileSearchInput.length) && ($desktopSearchInput.length)) {
            //pass to mobile
            $desktopSearchInput.keyup(function() {
                $mobileSearchInput.val($desktopSearchInput.val());
            });
            //pass to desktop
            $mobileSearchInput.keyup(function() {
                $desktopSearchInput.val($mobileSearchInput.val());
            });
        }
    },
    moveURLLabel: function() {
        var $urlLink = $('.url-link'),
            $hyperlinkList = $('.custom-df-hyperlink-item-link');
        if(monash_site.isMobilePhone() && ($urlLink.length && $hyperlinkList.length)) {
            $urlLink.insertBefore($hyperlinkList);
        }
    },
    moveTermsLabel: function() {
        var $termsLabel = $('.seForm_rows--terms table label'),
            $required = $('.seForm_rows--terms .se-form-required-text');

        if($termsLabel.length && $required.length) {
            $termsLabel.append($required);
        }    
    },
    tidyUpLabels: function() {
        var $addToLabel = $('.addToLabel');

        if($addToLabel.length) {
            
            $($addToLabel).each(function(){
                if($(this).prev().is('label')) {
                    $(this).prev().append($(this));
                }
            });
        }
    },
    tidyUpHome: function() {
        if ($('.no-flexbox').length) {
            var $homeEventsLI = $('.home .events li'),
                $homeEventsLILinks = $('.home .events li a'),
                $columnEqualizerChild = $('.columnEqualizer_child'),
                params;

            if(monash_site.isMobileDevice()) {
                if ($homeEventsLI.length) {
                    $homeEventsLILinks.height('auto');
                }
                if ($columnEqualizerChild.length) {
                    $columnEqualizerChild.height('auto');
                }
            } else {
                if ($homeEventsLI.length) {
                    params = [$homeEventsLILinks];
                    monash_site.columnEqualizer(params);
                }
                if ($columnEqualizerChild.length) {
                    params = [$('.columnEqualizer_row--1 .columnEqualizer_child'), $('.columnEqualizer_row--2 .columnEqualizer_child')];
                    monash_site.columnEqualizer(params);
                }
            }
        }
    },
    columnEqualizer: function(itemToEqualize) {
        for (var i = 0; i < itemToEqualize.length; ++i) {
            var maxHeight = 0;

            itemToEqualize[i].each(function(){
                if ($(this).height() > maxHeight) { 
                    maxHeight = $(this).height();
                }
            });
            //get the top and bottom padding (CSS if css is using box-sizing: border-box;)
            var topPadding = parseInt(itemToEqualize[0].css("padding-top")),
                bottomPadding = parseInt(itemToEqualize[0].css("padding-bottom")),
                maxHeightWithPadding = topPadding + maxHeight + bottomPadding;
                //console.log("top:" + topPadding + " bottom:" + bottomPadding + " maxHeight:" + maxHeight);
            itemToEqualize[i].height(maxHeightWithPadding);
        }
    },
    safariMobileSelectFix: function() {
        //fix a special bug in mobile safari, see https://discussions.apple.com/message/23745665#23745665
        var multiSelect = $('select[multiple]');
     
        multiSelect.on('blur',function(){
            var me = $(this);
            me.data('value',me.val());
            
            setTimeout(function(){
                me.val(me.data('value'));
            },600);
        });
    },
    removeEmptyErrorUL: function() {
        if((($('ul.seForm_errors').length) && ($('.seForm_errors li').length === 0))) { 
            $('.seForm_errors').remove();
        }
    },
    tidyUpSearchResults: function() {
        //move file-info into heading
        //move file size into the h2 element of list results
        var $listItemContainers =  $('.listing.site-search');

        if($listItemContainers.length) {
            $('a.document', $listItemContainers).each(function(){
                var context = $(this);
                var fileSize = $('span.file-info', this);
                var fileName = $('h3', this);
                fileSize.appendTo(fileName);
            });
        }
        //track the Sort list to get active class to 'Active' item 
        //read URL - don't faff with cookies
        // ... this gets ugly real fast ...
        var $searchSortLink = $('.searchSort_link'),
            $searchTypeForm = $('.searchSort--Forms'),
            $searchTypeHalls = $('.searchSort--Halls'),
            $searchTypeParks = $('.searchSort--Parks'),
            $searchTypeComm = $('.searchSort--Comm');

        if ($searchSortLink.length) {
            var pathname = window.location.href,
                pathCheck = '',
                splitter = "&sort_DLV%20Public%20Search%20Results="; //default

            if($searchTypeForm.length) {
                splitter = "sort_DLV%20Public%20Form%20Policy%20Plan=";
            } else if($searchTypeHalls.length) {
                //... re%20-%20GMAP=(sort_DLV%20Public%20Halls%20for%20Hire=Title|ascending)
                splitter = "sort_DLV%20Public%20Halls%20for%20Hire=";
            } else if($searchTypeParks.length) {
                splitter = "sort_DLV%20Public%20Parks=";
            } else if($searchTypeComm.length) {
                splitter = "sort_DLV%20Public%20Community="; //MONASH-1441
            }

            pathCheck = monash_site.pathToTrim(pathname, splitter);
            
            //console.log(pathCheck);
            
            //strip active Class
            $searchSortLink.removeClass('searchSort_link--active');

            switch(pathCheck) {
                case 'Title|ascending':
                    $('.searchSort a[data-sort="titleAZ"]').addClass(' searchSort_link--active');
                    break;
                case 'Title|descending':
                    $('.searchSort a[data-sort="titleZA"]').addClass(' searchSort_link--active');
                    break;
                case 'Activate%20Date|descending':
                    $('.searchSort a[data-sort="newest"]').addClass(' searchSort_link--active');
                    break;
                case 'Activate%20Date|ascending':
                    $('.searchSort a[data-sort="oldest"]').addClass(' searchSort_link--active');
                    break;
                default:
                    //assumes default - pop active class back on first link
                    $('.searchSort a').first().addClass('searchSort_link--active');
                    break;
            }
        }
        //... and done ... *breathe out*
    },
    pathToTrim:function(pathname, splitter) {
        var pathCheckToTrim = pathname.split(splitter)[1],
            pathCheck = '';
        if (typeof pathCheckToTrim != 'undefined') {
            pathCheck = pathCheckToTrim.replace('&filterRedirect=1', '');
        }
        return pathCheck;
    },
    tidyUpResizeControls: function() {
        $('.btnTextIncrease, .btnTextDecrease').val('A'); 
    },
    tidyUpSelectDropdowns:function() {
        $('.my-suburb select').chosen({
            width:"100%",
            disable_search:"true"
        });
    },
    tidyUpSynonym: function() {
        var $showMore = $('.show-more-synonym'),
            $showLess = $('.show-less-synonym');

        if ($('.synonym-container').length > 0) {
            $('.synonym-results li:lt(3)').addClass('synonymResults__item--show');

            if($('.synonym-results li').length > 3) {
               //show show more link
                $showMore.show();
                monash_site.collapseSynonyms();
                $showMore.click(function () {
                    //expand results
                    monash_site.expandSynonyms();
                    //hide this link
                    $(this).hide();
                    //show show less link
                    $showLess.show();
                });
                $showLess.click(function () {
                    //collapse
                    monash_site.collapseSynonyms();
                    //hide this link
                    $(this).hide();
                    //show show more link
                    $showMore.show();
                }); 
            }
        }
    },
    tidyUpTickIcons:function() {
        var $iconTick = $('.icon-tick');
        
        if($iconTick.length) {
            $iconTick.parent().addClass('successMessage_withIcon');
        }
    },
    tidyUpTabs:function() {
        var $tabList = $('.tabList'),
            $tabList_item = $('.tabList_item');
        //trying to avoid adding style - but might be a smarter method
        if($tabList_item.length) {
            $tabList_item.removeClass('addHeight, addExtraHeight');
            if ($tabList.height() > 55 && $tabList.height() <= 85) {
                $tabList_item.addClass('addHeight');
            } else if($tabList.height() > 85) {
                $tabList_item.addClass('addExtraHeight');
            }
        }
        //if has hash load it up
        if(window.location.hash) {
            var hash = window.location.hash;
            $tabList_item.each(function(i) {
                if($(this).find('a').attr('href') === hash) {
                    $(this).find('a').click();
                }
            });
        }
    },
    tidyUpMyCouncil:function() {
        var $mPanel = $('.managementPanel');

        if((monash_site.isMobileDevice()) && ($mPanel.length)) {
            $mPanel.each(function(i) {
                var $mPanelLink =  $(this).find('.link-button'),
                    $mPanelIntro = $(this).find('.top-details p'),
                    $mPanelLocation = $(this).find('.toggleMobilePhoneContainer_toggled');

                $mPanelLocation.prepend($mPanelLink);
                $mPanelLocation.prepend($mPanelIntro);
            });
        }
    },
    tidyUpLanding:function() {
        var $landingPageLinks = $('.landingPage_item a');

        if($landingPageLinks.length) {
            $landingPageLinks.matchHeight();
        }
    },
    tidyUpFeaturedNews:function() {
        var $newsitems = $('.newsTopStories .panelList_item'),
            $newsitemFirst = $('.newsTopStories .panelList_item--first a'),
            $newsitemSec = $('.newsTopStories .panelList_item:nth-of-type(2) a'),
            $newsitemThird = $('.newsTopStories .panelList_item:nth-of-type(3) a'),
            maxHeightFirst = 544,
            maxHeightRest = 470 + 25,
            maxHeightRestInd = 235,
            resizeHeight;


        $(function() { // wait till doc.ready to download images etc
            //console.log( "ready!" );
            if ($newsitems.length === 3) {
                var newsitemFirstH = $newsitemFirst.height(),
                    newsitemSecH = $newsitemSec.height(),
                    newsitemThirdH = $newsitemThird.height(),
                    newsitemsRestH = newsitemSecH + newsitemThirdH + 25,
                    newsitemsRestHComp = newsitemsRestH + 50; //allow for internal padding

                if ((newsitemFirstH > maxHeightFirst) && (newsitemFirstH > newsitemsRestHComp)) {
                    //the first one is too tall
                    resizeHeight = (newsitemFirstH-25-50)/2; //allowing for margin and padding
                    //resize the others
                    $newsitemSec.height(resizeHeight);
                    $newsitemThird.height(resizeHeight);
                } else if (newsitemsRestH > maxHeightRest) {
                    //the others too tall
                    resizeHeight = newsitemsRestH+50; //allowing for padding (includes margin)
                    //resize the first
                    $newsitemFirst.height(resizeHeight);
                }
            } 

        });   
    },
    clearKeyboard:function() {
        var $inputSearch = $('input.ps-input');
        $inputSearch.blur();
        console.log('keyboard cleared');
    },
    collapseSynonyms:function() {
        $('.synonym-results li:gt(2)').removeClass('synonymResults__item--show');
    },
    expandSynonyms:function() {
        //show all 
        $('.synonym-results li').addClass('synonymResults__item--show');
    }, 
    toggleAddLinkForm:function() {
        var $addLinkForm = $('.add-link-ext-container'),
            $addLinkButton = $('<a href="#" id="add-new-link" class="button">Add New Link</a>');
        if($addLinkForm.length) {
            // hide
            //if there are no errors
            if($('aside .add-link .errors li').length < 1) {
                $addLinkForm.addClass('visuallyhidden');
            }
            //add button
            $addLinkForm.before($addLinkButton);
            //add button action
            $addLinkButton.on("click", function(event) {
                event.preventDefault();
                $addLinkForm.toggleClass('visuallyhidden');
            });
        }
    },
    toggleHelp:function() {
        //use qTip
        var $helpToggle = $('.help-toggle .icon');
        $('.icon-info-toggler').click(function(e){
            e.preventDefault();
        });

        if(monash_site.isMobilePhone()) {
            //small version
            $('.icon-info-toggler').each(function() {
                //$.fn.qtip.plugins.iOS = false;
                $(this).qtip({
                    show: 'focus mouseover',
                    hide: 'blur mouseout',
                    content: {
                        text:$(this).next('.info-text')
                    },
                    style: {
                        tip: {
                            width: 23,
                            height: 12
                        }
                    },
                    position: {
                        my: 'top right',
                        at: 'bottom right',
                        target: $(this)
                    }
                });
            });    
        } else {
            $('.icon-info-toggler').each(function() {
                $(this).qtip({
                    show: 'focus mouseover',
                    hide: 'blur mouseout',
                    content: {
                        text:$(this).next('.info-text')
                    },
                    style: {
                        tip: {
                            width: 23,
                            height: 12,
                            mimic: 'center'
                        }
                    },
                    position: {
                        my: 'top right',
                        at: 'bottom right',
                        adjust: {
                            x:23,
                            y:8
                        }
                    }
                });
            });
        }
    },

    toggleMobileMainNav: function() {
        var monashSlidebars = new $.slidebars({
            scrollLock: true
        });

        monash_site.orientationChange(monashSlidebars);

        //$mobileMenuButton = $('.sb-toggle-right'),
            //$mobileMenu = $('nav.collapse'),
        var $toggleChildNav = $('.mobileMenu .icon-angle-down');
            //$backToTop = $('#back-to-top'),
            //$searchInput = $('input.ps-input');

        $toggleChildNav.on('click', function() {
            $(this).parent().toggleClass('mobileMenu-child--active');
            monash_site.clearKeyboard();
        });
    },
    addToggles: function() {
        //generic toggler (switched to BEM)
        var $toggleContainer = $('.toggleContainer');

        $toggleContainer.on('click', '.toggleContainer_toggler', function (e) {
            e.preventDefault();
            $(this).parents('.toggleContainer').toggleClass('toggleContainer--active');
            if($('aside div.sticky').length) { //add wiggle to force the sticky sidebar to move up
                var currentPostion = $(document).scrollTop();
                $('html,body').scrollTop(currentPostion + 1).scrollTop(currentPostion - 1);
            }
        });

        //should the toggler be open?
        monash_site.openToggles();
    },
    openToggles: function() {
        //console.log("checking if should open");
        var $toggleContainer = $('.toggleContainer');

        $toggleContainer.each(function(i) {
            //console.log("searching container: " + i);
            if(($(this).find('.successMessage').length) || ($(this).find('.seForm_errors').length)) {
                $(this).addClass('toggleContainer--active');
                //monash_site.tidyUpTickIcons();
            }
        });
    },
    addMobileToggles: function() {
        //Mobile only toggler (to be updated to BEM)
        //Should be replaced by below function
        $('.toggle-container').on('click', '.toggler', function (e) {
            //console.log("you are a mobile device - lets toggle" + $(this));
            if(monash_site.isMobileDevice()) {
                e.preventDefault();
                $(this).parent().toggleClass('active');
            }
        });
        if(monash_site.isMobileDevice()) {    // disable link for mob device
            $('.toggle-container').on('click', '.toggler a', function (e) {
                e.preventDefault();
            });    
        }
    },
    addMobilePanelToggles: function() {
        $('.toggleMobileContainer').on('click', '.toggleMobileContainer_toggler', function (e) {
            if(monash_site.isMobileDevice()) {
                e.preventDefault();
                $(this).parent().toggleClass('toggleMobileContainer--active');
            }
        });
    },
    addMobilePhonePanelToggles: function() {
        var $toggler = $('.toggleMobilePhoneContainer_toggler'),
            $toggleContainer = $('.toggleMobilePhoneContainer'),
            $arrowDown = $('<i/>')
                .addClass('icon-arrow-down show-for-small-down');

        //add arrows - if missing
        $toggler.not(':has(.icon-arrow-down)').append($arrowDown);
   
        //toggle
        $toggleContainer.on('click', '.toggleMobilePhoneContainer_toggler', function (e) {
            if(monash_site.isMobilePhone()) {
                e.preventDefault();
                //cos using parent is dumb
                var $myToggleContainer = $(this).parents('.toggleMobilePhoneContainer');
                $myToggleContainer.toggleClass('toggleMobilePhoneContainer--active');
            }
        });
    },
    toggleListViews: function() {
        var $listViewItem = $('#listView li'),
            $mapView = $('#mapView'),
            $listButtonsLink = $('.switchView a'),
            $pagination = $('.seamless-pagination'),
            $sortList = $('.searchSort');

            //deleted cookie M: 1554

        if($('#listView').length) {
            $listButtonsLink.on('click', function (e) {
                e.preventDefault();
                if(!$(this).hasClass('seForm_button--active')) { // current button is not active
                    $listButtonsLink.toggleClass('seForm_button--active');
                    //this always assumes that the List view is active
                    $listViewItem.toggleClass('visuallyhidden');
                    $mapView.toggleClass('visuallyhidden');
                    $pagination.toggleClass('visuallyhidden');
                    $sortList.toggleClass('visuallyhidden');
                }
            });
        }
    },
    resizeHandler:function() {
        //cheat to run function if user... ie tester is resizing the window
        //look at using timer? - http://goo.gl/zU2PB9

        //monash_site.addToggles();
        $(window).resize(function() {           
            delay(function() {
                monash_site.isMobileDevice();
                monash_site.tidyUpTabs();
                monash_site.tidyUpMyCouncil();
                monash_site.tidyUpHome();
            }, 500);            
        });
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
    },
    orientationChange:function(monashSlidebars) {
        $(window).on('orientationchange', function(event) {
            console.log('ori - change - updated');
            monash_site.clearKeyboard();
            //monash_site.fixLayoutAndroidBrowser();
            //monashSlidebars.slidebars.close(); v.1.0 only
            monashSlidebars.slidebars.remove();
            var monashSlidebars = new $.slidebars({
                scrollLock: true
            });
            console.log('giving up and closing');
        });
    },
    fixLayoutAndroidBrowser:function() {
        //if android only
        //temp reset size
        //?add class then remove it?
        console.log('fixAndriod - fired');
        $("body").addClass("not-fixed").delay(200).queue(function(next){
            console.log('fixAndriod - not-fixed removed');
            $(this).removeClass("not-fixed");
            next();
        });

    },
    fadeOutSuccess:function() {
        var $thankyou = $('.thank-you-message');
        if($thankyou.length) {
            $thankyou.delay(1500).animate({
                backgroundColor: "#F1F1F1"
            });
        }
    },
    isMobileDevice:function() {
        //CSS check that its smaller than 992px (landscape or portrait)
        //var $orientation = $('.orientation');

        if($('html.breakpoint-sc-size-1').length) {
            return false;
        } else {
            return true;
        }
    },
    isMobilePhone:function() {
        if(($('html.breakpoint-sc-size-1').length) || ($('html.breakpoint-sc-size-2').length)) {
            return false;
        } else {
            return true;
        }
    }
};

monash_site.init();