$(document).ready(function() {
    
    $('#main-nav').meanmenu();
    /* $('.minutes-table').addClass('responsive'); */
    $('.flexslider').flexslider({
    	animation: "slide",
    	itemMargin: 0,
    	animationLoop: true,
    	slideshow: false
	});
	
	if ($(".have-your-say-list")[0]){
		$('h1').addClass('no-border');
	}

	$('.add-to-calendar-trigger').removeClass('calOpen');
	$('.add-to-calendar-trigger').click( function () {
		$(this).toggleClass('calOpen');
	});
	
	if ($(".twitter-container")[0]){
		$('.rss-news').show();
	}
    
    if ($(".child-page-list-1")[0]){
    	if ($('.generic-doc-list').length = 0) {
    		$('.page-content').removeClass('page-content');
    		$('aside').hide();
    	  $(".child-page-list-1").removeClass(".child-page-list-1").addClass(".child-page-list-2");
    	}
    }
    
    $('ul.item-list li a').each(function() {
    if ($(this).find('img').length) {
        $(this).addClass('has-img');
    	}
	});
    
	$('.panel-list .hide-panel').hide();
	$('.accordion .subsribe-form').hide();
	$('#addToCalendarBody').hide();
	
	$('.click-to-view a').click( function () {
		$('.image-gallery a').first().click();
	});
	
	$('.add-to-calendar-trigger').click( function()  {
	  	$('#addToCalendarBody').toggle();
	});
	
	$('.panel-list h3').click( function (){
	  var panelID = $(this).attr('id');
	  $(this).toggleClass('open');
	  $('#'+panelID+'-panel').toggle();
    });
    
    $('.accordion h3').click( function () {
      	$('.accordion .subsribe-form').toggle();
      	$('.accordion h3').toggleClass('openform');
    });
    
    InitPredictiveSearch();
    InitPlaceHolder();
    InitTooltip();

  $menuItems = $('#main-nav ul > li a');
  $menuItems.focusin(function(){    
    $this = $(this);
    $this.parents('li.nav-has-children').addClass('sfHover');
  });
  $menuItems.focusout(function(){
    $menuItems.parents('li.nav-has-children').removeClass('sfHover');
  });
  $menuItems.keydown(function(e){
    $this = $(this);
    if($this.is(':focus'))
    {
      if(e.keyCode == 38 || e.keyCode == 37) //KEY_UP_ARROW or LEFT
		{
		simulateTab(this, 'prev');
		      e.preventDefault();
		}
		else if(e.keyCode == 40 || e.keyCode == 39) //KEY_DOWN_ARROW OR RIGHT
		{
		simulateTab(this, 'next');
		      e.preventDefault();
		} 
    }
 function simulateTab(currentElement, direction)
  {
    
        for (var j = 0, jl = $menuItems.length; j < jl; j++) {
            if (currentElement === $menuItems[j]) {
                if(direction == 'next')
        {
        if (j+1 < jl) {
                    $($menuItems[j+1]).focus();
                    $($menuItems[j+1]).trigger('focusin');
                } else {
                    $(currentElement).blur();
                }
        }
        else
        {
        if (j-1 >= 0) {
                    $($menuItems[j-1]).focus();
                    $($menuItems[j+1]).trigger('focusin');
                } else {
                    $(currentElement).blur();
                }
        }
            }
        }

    }
    });

    var titleSort = $(".btn_scSortingNonJS").first();
    var dateSort = $(".btn_scSortingNonJS").last();
    $(titleSort).attr("value", "Title");
    $(dateSort).attr("value", "Publish Date");

  // Tabs code - searches for a div with class "tabs" and applies JQuery UI
  // tabs() class
  // Will use cookies to remember state if div also has the class "useCookie"
 $(".tabs").each(function (index) {
    if ($(this).hasClass("useCookie")) {
      if($(this).attr("id") == ""){
        $(this).tabs({ cookie: { expires: 7 }});
      }else{
        $(this).tabs({ cookie: { expires: 7, name: $(this).attr("id") }});
      }
    }
    else {
      $(this).tabs();
    }
  });
  $(".tabs").on("tabsactivate", function (event, ui) {
    if (ui.newPanel.hasClass("withMap")) {

      ui.newPanel.find(".google-map-list").css("height","").css("width","");
      ui.newPanel.find(".google-map-list").seamlessGoogleMap('refresh');
    }
  });
	
  // Check box drop down list setup
  $('.check-box-drop-down').each(function () {
    if (this.dropdownCheckboxlistApplied) return;
    this.dropdownCheckboxlistApplied = true;
    
    $(this).prepend("<a class='option-trigger' href='#'><span class='icon-down'></span><span class='value'></span></a>");
    $(this).append("<div class='value-list'></div>");
    $(this).children('table')
      .appendTo($(this).children('.value-list'));
    $(this).show();
    
    $(this).height($(this).children('.option-trigger').outerHeight());
    var defaultValue = $(this).attr('data-default');
    
    if (defaultValue) {
      $(this).find('.value').text(defaultValue);
    }else {
      defaultValue ='';
    }
    
    $(this).bind('selection-change',function() {
      var selectedText = [];
      
      $(this).find('input[type=checkbox]:checked + label').each(function () {
        selectedText.push($(this).text());
      });
      
      if (selectedText.length > 0) {
        var newtext = selectedText[0];
        
        for (var i = 1; i < selectedText.length; i++) {
          newtext += ',' + selectedText[i];
        }
        
        $(this).find('.value').text(newtext);
      }
      else {
        $(this).find('.value').text(defaultValue);
      }
    });
    
    $(this).trigger('selection-change');
    
    $(this).children('.option-trigger').click(function(event) {
      var hasclass = $(this).parent().hasClass('open');
      $('.check-box-drop-down.open').removeClass('open');
      if(hasclass) {
        $(this).parent().removeClass('open');
      }
      else {
        $(this).parent().addClass('open');
      }
      event.preventDefault();
      return false;
    });
    
    $(this).find('input[type=checkbox],label').click(function(event) {
      $(this).closest('.check-box-drop-down').trigger('selection-change');
      event.stopPropagation();
    });
    
    $(this).find('ul, table').hover(function() {
      // in
      if(this.hidetimer) {
        clearTimeout(this.hidetimer);
        this.hidetimer = null;
      }
    },function() {
      // out
      var $this = this;
      this.hidetimer = setTimeout(function() {
        $this.hidetimer = null;
        $($this).closest('.check-box-drop-down').removeClass('open');
      }, 9000);
    });
    
  });
  
  $(document).click(function() {
    $('.check-box-drop-down.open').removeClass('open');
  });
  
});

if (($(window).width()) > 767) { 

$(function () {
		
    $('.seamless-pagination').each(function () {
        var pages = 10;
        var currentPage = 1;
        var displayedPages = 4;
        var edges = 1;
        var container = $(this);
        container.find('.seamless-pagination-no-js').each(function () {
            pages = $(this).find('option').length;
            currentPage = $(this).find('option:selected').val();
            $(this).css('display', 'none');
            return;
        });
        if (pages == "0") {
        	return;
        }
        container.find('.seamless-pagination-js').pagination({
            pages: pages,
            displayedPages: displayedPages,
            edges: edges,
            currentPage: currentPage,
            selectOnClick: false,
            cssStyle: 'light-theme',
            prevText: 'Previous',
            nextText: 'Next',
            onPageClick: function (pageNumber, event) {
                OnPageClick(pageNumber, container);
                event.preventDefault();
            }
        });
    });
});

}

function OnPageClick(pageNumber, container) {
    $(container).find('.seamless-pagination-no-js select').val(pageNumber);
    $(container).find('.seamless-pagination-no-js input').click();
}


//PREDICTIVE SEARCH STARTS
function InitPredictiveSearch() {
    PredictiveSearch({
        'searchWrap': '.searchform',
        'searchInput': '.searchform .ps-input',
        'searchButton': '.searchform .ps-button',
        'searchdlvname': 'RSS Public Search 2',
        'resultItem': 'a.predictive-search-link-item',
        'outputdiv': '#predictiveResults',
        'textDiv': '.predictive-search-abstract',
        'titleDiv': '.predictive-search-title',
        'textLimit': 80,
        'titleLimit': 28
    });
}

/*!
 * jQuery Predictive Search v1.1
 *
 * Copyright 2011, Seamless Solutions
 *
 * Date: Wed Dec 14, 2011 04:00PM
 */

function PredictiveSearch(settings) {
    // Settings to configure the jQuery Predictive Search plugin how you like
    settings = $.extend({
        //Seamless options
        'outputdiv': '#predictiveResults',
        'resultItem': 'li',
        'searchInput': '',
        'searchButton': '',
        'searchdlvname': '',
        'textDiv': '.searchFeedAbstract',
        'titleDiv': '.searchFeedTitle a',
        'textLimit': 100,
        'titleLimit': 50
    }, settings);

    var feedURL = '/feed.rss?listname=' + settings.searchdlvname + '&dlv_' + settings.searchdlvname + '=(wildcard_keyword=';
    var currentRequest = null;
    var thisObject = this;
    var timer = null;
	var keyword = "";

    try {
        $(settings.searchInput)[0].setAttribute("autocomplete", "off");
    } catch (ex) {}
    // 1. Listen to keystrokes in the textbox
    $(settings.searchInput).click(function () {
        //this.value='';
        this.setAttribute("autocomplete", "off");
    });

    $(settings.searchInput).blur(function (event) {
        $(settings.outputdiv).delay(1000).fadeOut(500, function () {
            this.innerHTML = '';
        });
    });

    $(settings.searchInput).focus(function (event) {
        $(settings.outputdiv).clearQueue();
        /*$(settings.outputdiv).show();*/
    });

    $(settings.searchInput).keypress(function (event) {
    //$(settings.searchInput).keydown(function (event) {
        // Ping for result

        if (event.which != 13 && event.which !== 0) {
            //thisObject.fetchResults(this.value + String.fromCharCode(event.which));
			if (timer) clearTimeout(timer);
			keyword = this.value + String.fromCharCode(event.which)
			timer = setTimeout(function()
			{
				thisObject.fetchResults(keyword);
			}, 500);
			
        } else {
            //check browser 
            //firefox support for tab
            if (navigator.userAgent.indexOf("Firefox") != -1 && event.which === 0) {
                //do nothing
            } else {
                event.stopPropagation();
                event.preventDefault();
            }
        }
    });

    $(settings.searchInput).keydown(function (event) {
        if (event.which == 13) {
            event.stopPropagation();
            event.preventDefault();
            event.cancelBubble = true;
            event.returnValue = false;
            return false;
        }
    });

    $(settings.searchInput).keyup(function (event) {
        if (event.which == 40) {
            // Down Arrow
            thisObject.moveBy(1);
        } else if (event.which == 38) {
            // Up Arrow
            thisObject.moveBy(-1);
        } else if (event.which == 8 || event.which == 46) {
            // Backspace
            //thisObject.fetchResults(this.value);
        } else if (event.which == 37) {
            // Left Arrow
            var currentIndex = doGetCaretPosition($(settings.searchInput)[0]);
            //Firefox support
            if (navigator.userAgent.indexOf("Firefox") != -1) {
                currentIndex -= 1;
            }
            setCaretPosition($(settings.searchInput)[0], currentIndex);
        } else if (event.which == 39) {
            // Right Arrow
            var currentIndex = doGetCaretPosition($(settings.searchInput)[0]);
            //Firefox support
            if (navigator.userAgent.indexOf("Firefox") != -1) {
                currentIndex += 1;
            }
            setCaretPosition($(settings.searchInput)[0], currentIndex);
        } else if (event.which == 13) {
            // Enter Key
            event.stopPropagation();
            event.preventDefault();
            if ($(settings.outputdiv + ' ' +
                settings.resultItem + '.itemSelected').length > 0) {
                thisObject.navigateOut();
            } else {
                try {
                    $(settings.searchButton)[0].click();
                } catch (ex) {}
            }
        }
    });

    thisObject.moveBy = function (diff) {
        var selectedItem = $(settings.outputdiv + ' ' + settings.resultItem + '.itemSelected');
        var selectedIndex = -1;
        var allItems = $(settings.outputdiv + ' ' + settings.resultItem);
        if (selectedItem.length > 0) {
            selectedIndex = $(settings.outputdiv + ' ' + settings.resultItem).index(selectedItem[0]);
        }
        var nextIndex = selectedIndex + diff;
        nextIndex += allItems.length;
        nextIndex %= allItems.length;
        thisObject.moveTo(allItems[nextIndex]);
    };

    thisObject.moveTo = function (item) {
        $(settings.outputdiv + ' ' + settings.resultItem).removeClass('itemSelected');
        item.className += ' itemSelected';
    };

    thisObject.navigateOut = function () {
        var selectedItem = $(settings.outputdiv + ' ' + settings.resultItem + '.itemSelected');
        document.location = $(selectedItem[0]).attr("href");
    };

    thisObject.fetchResults = function (keyword) {
        if (currentRequest !== null) {
            currentRequest.abort();
        }

        if (keyword.length < 3) {
            $(settings.outputdiv).html('');
            $(settings.outputdiv).hide();
            return;
        }

        $(settings.outputdiv).show();
        $(settings.outputdiv).html('<div class="loading" style="padding:12px 15px 15px 15px;">Loading...</div>');
        thisObject.currentIndex = 0;

        currentRequest = $.ajax({
            url: feedURL + keyword + '*)',
            type: "GET",
            dataType: "html",
            context: document.body,
            success: function (data) {
                $(settings.outputdiv).html(data);
                if (data === "") {
                    $(settings.outputdiv).hide();
					//$(settings.outputdiv).html('<p>No result.</p>');
                    return;
                }
                $(settings.outputdiv + ' ' + settings.resultItem).mouseover(function () {
                    thisObject.moveTo(this);
                });
                $(settings.outputdiv + ' ' + settings.textDiv).each(function (index, item) {
                    item.innerHTML = (item.innerHTML.length > settings.textLimit) ? item.innerHTML.substring(0, settings.textLimit) + "..." : item.innerHTML;
                });
                $(settings.outputdiv + ' ' + settings.titleDiv).each(function (index, item) {
                    item.innerHTML = (item.innerHTML.length > settings.titleLimit) ? item.innerHTML.substring(0, settings.titleLimit) + "..." : item.innerHTML;
                });
								
				$linkViewMore = $(settings.outputdiv).find('.view-more a');
				if($linkViewMore.length == 1)
				{
					var linkViewMoreHref = $linkViewMore.attr('href').replace('{0}', keyword);
					$linkViewMore.attr('href', linkViewMoreHref);
				}				
            },
            error: function () {
                //$(settings.outputdiv).html('<p>Error retrieving results from server</p>');
            }
        });
    };
}

function doGetCaretPosition(ctrl) {
    var CaretPos = 0;
    // IE Support
    if (document.selection) {
        ctrl.focus();
        var Sel = document.selection.createRange();
        Sel.moveStart('character', -ctrl.value.length);
        CaretPos = Sel.text.length;
    }
    // Firefox support
    else if (ctrl.selectionStart || ctrl.selectionStart == '0')
        CaretPos = ctrl.selectionStart;
    return (CaretPos);
}

function setCaretPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(pos, pos);
    } else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}
//PREDICTIVE SEARCH ENDS

//TOOLTIP
function InitTooltip()
{
    $('.tooltip a').tooltip();
}
//TOOLTIP ENDS

//PLACE HOLDER

function InitPlaceHolder()
{
    $('.placeholder-select > select').placeHolder({
        'text': 'Select' //placeholder text
    });
}

// By Chris Coyier & tweaked by Mathias Bynens

$(function() {

	// Find all YouTube videos
	var $allVideos = $("iframe[src^='//www.youtube.com']"),

	    // The element that is fluid width
	    $fluidEl = $(".content");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {

		$(this)
			.data('aspectRatio', this.height / this.width)
			
			// and remove the hard coded width/height
			.removeAttr('height')
			.removeAttr('width');

	});

	// When the window is resized
	// (You'll probably want to debounce this)
	$(window).resize(function() {

		var newWidth = $fluidEl.width();
		
		// Resize all videos according to their own aspect ratio
		$allVideos.each(function() {

			var $el = $(this);
			$el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));

		});

	// Kick off one resize to fix all videos on page load
	}).resize();

});

/*
 * Place Holder JQuery Plugin
 * version 1.3
 * last update: 2013 Mar 28
 * author: Nick Huynh <nhuynh@seamless.com.au> + Mike Le <mle@seamless.com.au>
 */
(function($){$.fn.placeHolder=function(options){var settings=$.extend({"text":"Please provide the input","mColor":"#4d4d4d","tColor":"#000","submitButton":'.divContainer input[type="submit"]'},options);if(typeof String.prototype.trim!=="function")String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")};return this.each(function(){var $this=$(this);var placeholderText=$(this).attr("placeholder");if(placeholderText==null)placeholderText=$(this).parent().attr("placeholder");if(placeholderText!=
null)settings.text=placeholderText;if($(this)[0].nodeName.toLowerCase()==="select")if($this.children("option:first").text()=="")$this.children("option:first").text(settings.text);else $this.html("<option value=''>"+settings.text+"</option>"+$this.html());else{if($this.val()==false){$this.val(settings.text);$this.css("color",settings.mColor)}$this.blur(function(){if($this.val().trim().length==0){$this.val(settings.text);$this.css("color",settings.mColor)}else $this.css("color",settings.tColor)});$this.focus(function(){if($this.val()==
settings.text){$this.val("");$this.css("color",settings.tColor)}});$(settings.submitButton).click(function(){if($this.val()==settings.text)$this.val("")})}})}})(jQuery);

//PLACE HOLDER ENDS