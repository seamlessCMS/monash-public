/*
Butchered by Brent:
Seamless City main.js
 
Written by: Riz Iqbal and Iain Fraser
Version: 2.0
Date: 7 March 2014
Purpose: Establish main javascript functionality for Seamless City

Dependencies: 
    jQuery 1.10 and up (Version 2 and up not supported)
    Individual plugins have their own dependencies as documented
 
*/


var SeamlessCity = SeamlessCity || {};
(function ($sc) { $.extend($sc, { //<-- code concatenated to 1 line for readability

init : function()
{
    this.init = function(){};//Prevent multiple calls to init
    

    /*
    Multiform Fix for ASP.NET pages

    Written by: Iain Fraser
    Version: 1.0
    Date: 20 June 2013
    Purpose: This script... [SNIP] - refer to SC2 docs

     */
    function MultiformFix()
    {
        $(function () {

            var onInputEnter = function (e) {
                var type = $(this).attr('type');
                if (e.which == 13 && type != 'submit' && type != 'button') {

                    var traverse = $(this);
                    var button;

                    //Traverse through parents until button or existing handler is found
                    while ($(traverse).length) {
                        traverse = $(traverse).parent();

                        //Check for existing handler and just let that do its thing if it exists
                        //NOTE: IMPROVE THIS, Don't like relying on the function name being constant...
                        var okp = $(traverse).attr('onkeypress');
                        if (/^javascript:return WebForm_FireDefaultButton/.test(okp)) {
                            break;
                        }

                        //Try and find a submit button
                        button = $('input[type=submit]', traverse);
                        if ($(button).length) {
                            var hasDefault = $(button).filter('.default-button');
                            if (hasDefault.length) {
                                //Default button found
                                //Simulate click on default button
                                $(hasDefault).first().click();
                            } else {
                                //No default button found. Clicking first button
                                //Simulate click on first button
                                $(button).first().click();
                            }
                            e.preventDefault();
                            break;
                        }
                    }

                }
            };
            //Here, we are only binding keypress to our event handler when we detect a focus event though the form
            //This way we can bind to dynamically generated fields, and we don't have wasted keypress handlers on fields the user never focussed on
            $('form').delegate('input', 'focus', function () {
                if (!$(this).data('okpbound')) {
                    $(this).data('okpbound', true);
                    $(this).keypress(onInputEnter);
                }
            });
        });
    }
    
    
    MultiformFix();
    
    
    //$sc.Plugins.BreakpointChange.init();
    
},

/*
QueryString
Written by: Iain Fraser 
    with help from http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    and http://stackoverflow.com/questions/2878703/split-string-once-in-javascript (yarmiganosca)
Date: 23 January 2014
Description: Converts query string component of URL into an object
*/
QueryString :   (function(rawQueryString) {
                    if (rawQueryString === "") return {};
                    var querystringObject = {};
                    for (var i = 0; i < rawQueryString.length; ++i)
                    {
                        var keyValuePair = rawQueryString[i].split('=');
                        if (keyValuePair.length < 2) keyValuePair.push('');
                        if (keyValuePair.length > 2) keyValuePair = [keyValuePair.shift(), keyValuePair.join('=')];
                        querystringObject[keyValuePair[0]] = decodeURIComponent(keyValuePair[1].replace(/\+/g, " "));
                    }
                    return querystringObject;
                })(window.location.search.substr(1).split('&')),

/*
Tools
Description: Namespace for storing helper functions
*/
Tools : {
    /*
    Tools.Caret    
    Description: Tools for getting and setting the position of the caret in text boxes
    */
    Caret : {
        
        getPosition : function(ctrl) {
            var CaretPos = 0;
            // IE Support
            if (document.selection) {
                ctrl.focus();
                var Sel = document.selection.createRange();
                Sel.moveStart('character', -ctrl.value.length);
                CaretPos = Sel.text.length;
            }
            // Firefox support
            else if (ctrl.selectionStart || ctrl.selectionStart == '0')
                CaretPos = ctrl.selectionStart;
            return (CaretPos);
        },

        setPosition : function (ctrl, pos) {
            if (ctrl.setSelectionRange) {
                ctrl.focus();
                ctrl.setSelectionRange(pos, pos);
            } else if (ctrl.createTextRange) {
                var range = ctrl.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
            }
        }
    },
    /*
    Tools.ValueOrDefault
    Written by: Iain Fraser
    Date: 23 January 2014
    Description: Returns value if defined, otherwise returns defaultValue
    */
    ValueOrDefault : function(value, defaultValue)
    {
        return (typeof(value) == 'undefined') ? defaultValue : value;
    },
    
    /*
    Tools.GetQueryStringParameterByName
    Written by: Iain Fraser
    Date: 23 January 2014
    Description: Returns value of query string parameter given by 'name'
    */
    GetQueryStringParameterByName : function (name) {
        return $sc.QueryString[name];
    },
    

    ArgsAsOptions : function(args, argMapping)
    {
        var Options = {};
        var ArgMapping = argMapping || [];
        
        for(var i = 0; i < args.length; i++)
        {
            var argType = typeof(args[i]);
            var isLastArg = ( i === (args.length - 1) );
            var argTypeIsObject = (argType == 'object'); //Is the current argument an object?
            var argHasMapping = (ArgMapping.length >= (i+1) && typeof(ArgMapping[i]) == 'object'); 
            var argMatchesMappedType = (argHasMapping && typeof(ArgMapping[i][argType]) != 'undefined'); 
            var argIsMappedAsOptions = (argMatchesMappedType && argTypeIsObject && ArgMapping[i].object == 'options');

            if(argMatchesMappedType)
            {
                if(argIsMappedAsOptions)
                {
                    Options = $.extend(Options, args[i]);
                } else {
                    Options[ArgMapping[i][argType]] = args[i];
                }
            } 
            else if (argTypeIsObject && isLastArg)
            {
                Options = $.extend(Options, args[i]);                    
            } 
            else if (argHasMapping) 
            {
                //If the arg doesn't match the mapped type, but it does have a mapping
                
                var warn = 'ArgsAsOptions: Argument '+(i+1)+' ('+args[i]+') is not of the correct type. Type is \''+argType+'\'.';
                var expecting = '';
                var j = 0;
                for(var k in ArgMapping[i])
                {
                    if(j > 0)
                    {
                        expecting += ', ';
                    }
                    expecting += '\''+k+'\'';
                    j++;
                }
                expecting = (j == 1) ? ' Expecting: ' + expecting + '.' : ' Expecting one of: ' + expecting  + '.';
                console.warn(warn + expecting);
                return false;
            }
            
        }
        if(!Options.sc_selector && Options.selector)
        {
            Options.sc_selector = Options.selector;
        }
        
        return Options;
    },
    
    /*
    Tools.Debounce
    Written by: Unscriptable.com
    Date: 23 January 2014
    Description: Debouncing ensures that exactly one signal is sent for an event that may be happening several times
    http://unscriptable.com/2009/03/20/debouncing-javascript-methods/
    */
    
    Debounce : function (func, threshold, execAsap) {
     
        var timeout;
     
        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null; 
            }
     
            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);
     
            timeout = setTimeout(delayed, threshold || 100); 
        };
     
    }
},
/*
Dependencies
Description: Namespace for storing dependency management functions
*/
Dependencies : {
    /*
    Dependencies.Check
    Written by: Iain Fraser
    Date: 23 January 2014
    Description: Takes an list of dependency names and boolean values and the name of 
                 the calling object. Any dependencies set to false will generate a
                 console.warn indicating which dependencies are missing. If any
                 dependency is found to be false, this function will return false
                 to allow the calling function to do further processing.    
    Usage: 
        Check( object[, string] )   
        
            object:
                { string : boolean[, string : boolean[, string : boolean...]] }
                string: Name of dependency
                boolean: Dependency is available
            
            string:
                Fully qualified name of the calling object
                
    Returns: boolean
                true: all dependencies are true
                false: one or more dependencies are false
    
    */
    Check : function(d, callerName)
    {
        var okayToProceed = true;
        if(d)
        {
            
            for(var key in d)
            {
                if(!d[key])
                {
                    var message = 'A required dependency "'+key+'" is missing.';
                    message = (callerName) ? callerName + ': ' + message : message;
                    console.warn(message);
                    okayToProceed = false;
                }
            }
        }
        return okayToProceed;
    }
},

/*
Plugins
Description: Namespace for storing Plugins and their initialisers
*/
Plugins : {
    /*
    Plugins.Pagination
    Written by: Riz Iqbal and Iain Fraser
    Date: 23 January 2014
    Dependencies: simplePagination.js (http://flaviusmatis.github.io/simplePagination.js/)
    Description: Uses the jQuery plugin "Simple Pagination" to make clickable
                 page numbers out of the Seamless CMS default noJS pagination control 
                 (i.e. select box with action buttons). 
    Usage:
        SeamlessCity.Plugins.Pagination.init(string[, object])
        
            string: jQuery selector for element containing pagination
            object: options to be passed to Simple Pagination plugin
    */
    Pagination : {
        _options : {
            pages: 10,
            currentPage: 1,
            displayedPages: 3,
            edges: 1,
            cssStyle: 'light-theme',
            prevText: 'Previous',
            nextText: 'Next',
            selectOnClick: false
        },        
        init : function()
        {
            var Pagination = this;
            var me = 'SeamlessCity.Plugins.Pagination.init';
            
            
            var argMapping = [{'string':'sc_selector'}];
            var initOpts = $sc.Tools.ArgsAsOptions(arguments, argMapping);
            if(initOpts === false)
            {
                console.warn(me + ': failed');
                return false;
            }
            //Merge passed options into default options
            var opts = $.extend( {}, this._options, ( initOpts || {} ) );
            
            
            //Check for dependencies
            var depends = { 
                    
                    'jQuery Simple Pagination' : $.fn.pagination,
                    'selector for pagination' : opts.sc_selector
                };
            
            if( !$sc.Dependencies.Check(depends, me) ){ 
                //If dependencies aren't there, then enable noJS control
                $(opts.sc_selector).find('.seamless-pagination-controls').show();
                return false; 
            }            
            
            $(opts.sc_selector).each(function () {                    
                var container = $(this);
                
                //Add this specific instance of OnPageClick to our options
                var thisOpts = $.extend({}, opts, {
                        onPageClick : 
                            function (pageNumber, event) {
                                Pagination.SetPage(pageNumber, container);
                                event.preventDefault();
                            }
                });        

                //Count pages and get current page
                $('.seamless-pagination-data', container).each(function () {
                    thisOpts.pages = $('option', this).length;
                    thisOpts.currentPage = $('option:selected', this).val();
                });
                
                //If there are /actually/ pages here, then fire the pagination
                if(thisOpts.pages > 0)
                {
                    $('.seamless-pagination-js', container).pagination(thisOpts);
                }
            });
        },
        SetPage: function(pageNumber, container)
        {
            $('.seamless-pagination-data select', container).val(pageNumber);
            $('.seamless-pagination-data input', container).click();            
        }
    },
    //End Pagination
    
    PredictiveSearch : {
        _options: {
            searchWrap: '.search-form',
            searchInput: '.search-form .ps-input',
            searchButton: '.search-form .ps-button',
            searchdlvname: 'RSS Public Search',
            resultItem: 'a.predictive-search-link-item',
            outputdiv: '.predictive-results',
            moreResultsLocation : '/Lists/Search',
            moreResultsHyperlink : '.view-more',
            moreResultsListview : 'dlv_DLV Public Search Results' //M:1875
        },
        init : function()
        {
            this.init = function(){}; //Prevent init from being called again
            
            var PredictiveSearch = this;
            var me = 'SeamlessCity.Plugins.PredictiveSearch.init';
            var initOpts = $sc.Tools.ArgsAsOptions(arguments);
            var opts = $.extend( {}, this._options, ( initOpts || {} ) );    
            
            /*!
             * jQuery Predictive Search v1.1
             *
             * Copyright 2011, Seamless Solutions
             *
             * Date: Wed Dec 14, 2011 04:00PM
             */
            //Note from Iain: Turned this into a closure for now - our init wraps this functionality, so let's keep it private
            //Haven't refactored or standardised any of this - just moved function calls to their proper namespaces
            (function (settings) {
                // Settings to configure the jQuery Predictive Search plugin how you like
                settings = $.extend({
                    //Seamless options
                    'outputdiv': '.predictive-results',
                    'resultItem': 'li',
                    'searchInput': '',
                    'searchButton': '',
                    'searchdlvname': '',
                    'textDiv': '.predictive-search-abstract',
                    'titleDiv': '.predictive-search-title',
                    'textLimit': 100,
                    'titleLimit': 50,
                    'moreResultsHyperlink': '',
                    'moreResultsLocation': '',
                    'moreResultsListview': ''
                }, settings);

                var feedURL = '/feed.rss?listname=' + settings.searchdlvname + '&dlv_' + settings.searchdlvname + '=(wildcard_keyword=';
                var currentRequest = null;
                var thisObject = this;
                var timer = null;
                var keyword = "";

                try {
                    $(settings.searchInput)[0].setAttribute("autocomplete", "off");
                } catch (ex) {}
                // 1. Listen to keystrokes in the textbox
                $(settings.searchInput).click(function () {
                    //this.value='';
                    this.setAttribute("autocomplete", "off");
                });

                $(settings.searchInput).blur(function (event) {
                    $(settings.outputdiv).delay(1000).fadeOut(500, function () {
                        this.innerHTML = '';
                    });
                });

                $(settings.searchInput).focus(function (event) {
                    $(settings.outputdiv).clearQueue();
                    /*$(settings.outputdiv).show();*/
                });

                $(settings.searchInput).keypress(function (event) {
                    // Ping for result
                    if (event.which != 13 && event.which !== 0) {
                        //thisObject.fetchResults(this.value + String.fromCharCode(event.which));
                        if(event.which == 8)
                        {
                            return;
                        }
                        if (timer) clearTimeout(timer);
                        keyword = this.value + String.fromCharCode(event.which);
                        
                        timer = setTimeout(function()
                        {
                            thisObject.fetchResults(keyword);
                        }, 500);
                        
                    } else {
                        //check browser 
                        //firefox support for tab
                        if (navigator.userAgent.indexOf("Firefox") != -1 && event.which === 0) {
                            //do nothing
                        } else {
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    }
                });

                $(settings.searchInput).keydown(function (event) {
                    if (event.which == 13) {
                        event.stopPropagation();
                        event.preventDefault();
                        event.cancelBubble = true;
                        event.returnValue = false;
                        return false;
                    }
                });

                $(settings.searchInput).keyup(function (event) {
                    var currentIndex;
                    
                    if (event.which == 40) {
                        // Down Arrow
                        thisObject.moveBy(1);
                    } else if (event.which == 38) {
                        // Up Arrow
                        thisObject.moveBy(-1);
                    } else if (event.which == 8 || event.which == 46) {
                        // Backspace
                        //thisObject.fetchResults(this.value);
                        if (timer) clearTimeout(timer);
                        keyword = this.value;
                        thisObject.fetchResults(keyword);
                    } 
                    else if (event.which == 37) {
                        // Left Arrow
                        currentIndex = $sc.Tools.Caret.getPosition($(settings.searchInput)[0]);
                        //Firefox support
                        if (navigator.userAgent.indexOf("Firefox") != -1) {
                            currentIndex -= 1;
                        }
                        $sc.Tools.Caret.setPosition($(settings.searchInput)[0], currentIndex);
                    } else if (event.which == 39) {
                        // Right Arrow
                        currentIndex = $sc.Tools.Caret.getPosition($(settings.searchInput)[0]);
                        //Firefox support
                        if (navigator.userAgent.indexOf("Firefox") != -1) {
                            currentIndex += 1;
                        }
                        $sc.Tools.Caret.setPosition($(settings.searchInput)[0], currentIndex);
                    } else if (event.which == 13) {
                        // Enter Key
                        event.stopPropagation();
                        event.preventDefault();
                        if ($(settings.outputdiv + ' ' +
                            settings.resultItem + '.itemSelected').length > 0) {
                            thisObject.navigateOut();
                        } else {
                            try {
                                $(settings.searchButton)[0].click();
                            } catch (ex) {}
                        }
                    }
                });

                thisObject.moveBy = function (diff) {
                    var selectedItem = $(settings.outputdiv + ' ' + settings.resultItem + '.itemSelected');
                    var selectedIndex = -1;
                    var allItems = $(settings.outputdiv + ' ' + settings.resultItem);
                    if (selectedItem.length > 0) {
                        selectedIndex = $(settings.outputdiv + ' ' + settings.resultItem).index(selectedItem[0]);
                    }
                    var nextIndex = selectedIndex + diff;
                    nextIndex += allItems.length;
                    nextIndex %= allItems.length;
                    thisObject.moveTo(allItems[nextIndex]);
                };

                thisObject.moveTo = function (item) {
                    $(settings.outputdiv + ' ' + settings.resultItem).removeClass('itemSelected');
                    item.className += ' itemSelected';
                };

                thisObject.navigateOut = function () {
                    var selectedItem = $(settings.outputdiv + ' ' + settings.resultItem + '.itemSelected');
                    document.location = $(selectedItem[0]).attr("href");
                };

                thisObject.fetchResults = function (keyword) {
                    if (currentRequest !== null) {
                        currentRequest.abort();
                    }
                    if (keyword.length < 3) {
                        $(settings.outputdiv).html('');
                        $(settings.outputdiv).hide();
                        return;
                    }

                    $(settings.outputdiv).show();
                    $(settings.outputdiv).html('<div class="search-loading">Loading...</div>');
                    thisObject.currentIndex = 0;

                    currentRequest = $.ajax({
                        url: feedURL + keyword + '*)',
                        type: "GET",
                        dataType: "html",
                        context: document.body,
                        success: function (data) {
                            $(settings.outputdiv).html(data);
                            if (data === "") {
                                //$(settings.outputdiv).hide();
                                $(settings.outputdiv).html('<div class="no-results">No results found.</div>');
                                return;
                            }
                            $(settings.outputdiv + ' ' + settings.resultItem).mouseover(function () {
                                thisObject.moveTo(this);
                            });
                            $(settings.outputdiv + ' ' + settings.textDiv).each(function (index, item) {
                                item.innerHTML = (item.innerHTML.length > settings.textLimit) ? item.innerHTML.substring(0, settings.textLimit) + "..." : item.innerHTML;
                            });
                            $(settings.outputdiv + ' ' + settings.titleDiv).each(function (index, item) {
                                item.innerHTML = (item.innerHTML.length > settings.titleLimit) ? item.innerHTML.substring(0, settings.titleLimit) + "..." : item.innerHTML;
                            });
                            if(settings.moreResultsHyperlink !== '') {
                                // console.log("moreResults is successful");
                                // console.log("link to be targeted: " + settings.moreResultsHyperlink);
                                // console.log("keyword: " + keyword);
                                $(settings.outputdiv + ' ' + settings.moreResultsHyperlink).attr('href', settings.moreResultsLocation + '?' + settings.moreResultsListview + '=(keyword=' + keyword + ')');
                            }
                            //console.log('predictive-results success');
                            //HighlightText : function(element, val, options)
                            SeamlessCity.Plugins.HighlightText.HighlightText(settings.outputdiv + ' ' + '.predictive-search-link-item h3' ,keyword);
                            SeamlessCity.Plugins.HighlightText.HighlightText(settings.outputdiv + ' ' + '.predictive-search-link-item p' ,keyword);
                        },
                        error: function () {
                            $(settings.outputdiv).html('<div class="no-results">Error retrieving results from server</div>');
                        }
                    });
                };
            })(opts);
        }
    },
    //End PredictiveSearch
      //End Pagination
    

    /*
    Plugins.BackToTop
    Written by: Riz Iqbal and Iain Fraser
    Date: 23 January 2014
    Dependencies: 
    Description: 
    
    Usage:
        SeamlessCity.Plugins.BackToTop.init()
    */
    BackToTop : {
        _options: {
            selector : '#back-to-top',
            scrollSpeed : 800,
            threshold : 200,
            position : 'right',
            openPositionValue : '0',
            closedPositionValue : '-50px',
            autoHideTimeOut : 0
        },
        init: function()
        {   
        
            var BackToTop = this;
            var me = 'SeamlessCity.Plugins.BackToTop.init';
            
            
            var argMapping = [
                {'string' : 'selector'}
            ];      
            var initOpts = $sc.Tools.ArgsAsOptions(arguments, argMapping);
            if(initOpts === false)
            {
                console.warn(me + ': failed');
                return false;
            }
            //Merge passed options into default options
            var opts = $.extend( {}, this._options, ( initOpts || {} ) );        

            var bttObject = $(opts.selector);
            
            function hideBackToTop()
            {   
                bttObject.css(opts.position, opts.closedPositionValue); 
            }            
            
            function showBackToTop()
            {   
                bttObject.css(opts.position, opts.openPositionValue);   
            }

            hideBackToTop();
            
            var timeoutId;
            
            //Debouncing so event handler is only called once in 50 milliseconds
            $(window).scroll($sc.Tools.Debounce(function () {
                if(timeoutId)
                {
                    window.clearTimeout( timeoutId );
                    timeoutId = null;
                }
                if ($(this).scrollTop() > opts.threshold) {
                    showBackToTop();
                    if(opts.autoHideTimeOut)
                    {
                        timeoutId = window.setTimeout( function(){hideBackToTop();}, opts.autoHideTimeOut );
                    }
                } else {
                    hideBackToTop();
                }
            },50));

            bttObject.click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, opts.scrollSpeed);
                return false;
            });
        }
    },
    
        /*
    Plugins.HighlightText
    Written by: Riz Iqbal and Iain Fraser
    Date: 23 January 2014
    Dependencies: 
    Description: 
    
    Usage:
        SeamlessCity.Plugins.HighlightText.init()
    */
    HighlightText : {
        _options: {
            rhDataAttrHighlight : '[data-result-highlight]',
            rhDataAttrSource : '[data-result-highlight-source]',
            rhDataAttrElement : '[data-result-highlight-element]',
            highlightTagName : 'span',
            highlightClass : 'result-text'
        },
        init: function()
        {   
            var HighlightText = this;
            var me = 'SeamlessCity.Plugins.HighlightText.init';
            
            var initOpts = $sc.Tools.ArgsAsOptions(arguments);
            if(initOpts === false)
            {
                console.warn(me + ': failed');
                return false;
            }
            //Merge passed options into default options
            var opts = $.extend( {}, this._options, ( initOpts || {} ) );   
            $(opts.rhDataAttrHighlight).each(function () {
                var sourceText = $(this).find(opts.rhDataAttrSource + ' input[type="text"]').val();
                if (sourceText !== '') {
                    HighlightText.HighlightText(opts.rhDataAttrHighlight + ' ' + opts.rhDataAttrElement, sourceText, opts);
                }
            });
        },
        HighlightText : function(element, val, options)
        {            
            var opts = $.extend( {}, this._options, ( options || {} ) );
            opts._highlightTagNameClose = opts.highlightTagName.replace(/ .*/g, '');
            
            function ReplaceMatchedText(match)
            {
                return '<' + opts.highlightTagName + ' class="' + opts.highlightClass + '">' + match + '</' + opts._highlightTagNameClose + '>';        
            }
            $(element).each(function () {
                var htmlText = $(this).html();
                var valueToCheck = new RegExp(val, 'gi');
                var match = htmlText.match(valueToCheck);
                $(this).html(htmlText.replace(valueToCheck, ReplaceMatchedText));
            });
        }
    },
 
    
    /*
    Plugins.ResponsiveTables
    Written by: Riz Iqbal and Iain Fraser
    Date: 23 January 2014
    Dependencies: 
    Description: 
    
    Usage:
        SeamlessCity.Plugins.ResponsiveTables.init()
    */
    ResponsiveTables : {
        _options: {     
        },
        init: function()
        {              
            var ResponsiveTables = this;
            var me = 'SeamlessCity.Plugins.ResponsiveTables.init';

            var switched = false;
            var updateTables = function () {
                if (($(window).width() < 768) && !switched) {
                    switched = true;
                    $("table.sc-responsive-table").each(function (i, element) {
                        splitTable($(element));
                    });
                    return true;
                }
                else if (switched && ($(window).width() > 768)) {
                    switched = false;
                    $("table.sc-responsive-table").each(function (i, element) {
                        unsplitTable($(element));
                    });
                }
            };

            $(window).load(updateTables);
            $(window).on("redraw", function () { switched = false; updateTables(); });
            $(window).on("resize", updateTables);

            function splitTable(original) {
                original.wrap("<div class='table-wrapper' />");

                var copy = original.clone();
                copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
                copy.removeClass("sc-responsive-table");
                copy.addClass("sc-responsive-table-heading");

                original.closest(".table-wrapper").append(copy);
                copy.wrap("<div class='pinned' />");
                original.wrap("<div class='scrollable' />");

                setCellHeights(original, copy);
            }

            function unsplitTable(original) {
                original.closest(".table-wrapper").find(".pinned").remove();
                original.unwrap();
                original.unwrap();
            }

            function setCellHeights(original, copy) {
                var tr = original.find('tr'),
                    tr_copy = copy.find('tr'),
                    heights = [];

                tr.each(function (index) {
                    var self = $(this),
                      tx = self.find('th, td');

                    tx.each(function () {
                        var height = $(this).outerHeight(true);
                        heights[index] = heights[index] || 0;
                        if (height > heights[index]) heights[index] = height;
                    });

                });

                tr_copy.each(function (index) {
                    $(this).height(heights[index]);
                });
            }
        }
    }
          
}
});

$sc.init();


})(SeamlessCity);//<-- code concatenated to 1 line for readability

var SEAMLESS = SEAMLESS || {};
    (function (googleMaps, $, undefined) {
 
        googleMaps.initialise = function() {
            var $mapContainers = $('.gmap'),
                $breakpointXS = $('.breakpoint-sc-size-4'),
                $neighbourhoodMap = $('.neighbourhood-map-listing');
            
            if($breakpointXS.length) {
                if($neighbourhoodMap.length) {
                    $($mapContainers).each(function() {
                        InitialiseMap(this);
                    });    
                } else {
                    //don't load maps JS if mobile device   
                }   
            } else {
                $($mapContainers).each(function() {
                    if(!$mapContainers.hasClass('visuallyhidden')) {
                        InitialiseMap(this);
                    }
                });
            }   
        };
        
        function closeAllInfoWindows()
        {
            mainMap.infoWindow.close();
        }
        
        var markers = [];
        var mainMap;
        var oms;

        function InitialiseMap(mapContainer)
        {
            var $data = $(mapContainer).data();
            var $markerSourceElements = $(mapContainer).find('.gmap-marker');
            var targetDiv = $(mapContainer).find('.gmap-target')[0];
            $(targetDiv).find('img').remove();          
            mainMap = {
                container : mapContainer,
                markers : [],
                markerSourceElements : $markerSourceElements,
                isMultiple : $markerSourceElements.size() > 1,
                params : $data['params'],
                targetDiv : targetDiv,
                googleMap : CreateGoogleMap(targetDiv, $data['params']),
                infoWindow : new google.maps.InfoWindow()
            };
            
            mainMap.infoWindow.setOptions({maxWidth: 295});
            markers = [];
            oms = new OverlappingMarkerSpiderfier(mainMap.googleMap);
            if(mainMap.params.loadAsync == undefined || mainMap.params.loadAsync == false)
            {
                AddMarkers(mainMap);
                if(mainMap.params.fitBounds === true && markers.length > 0)
                {
                    SetBounds(mainMap);
                }
                var mcOptions = {
                  'zoom': 12, //M:1849 - was 13
                  'maxZoom': 15
                };
                var mc = new MarkerClusterer(mainMap.googleMap, markers, mcOptions);
                google.maps.event.addListener(mainMap.googleMap, "click", function () { 
                    closeAllInfoWindows();
                });
            }               
            else
            {           
                var mc = new MarkerClusterer(mainMap.googleMap, markers, mcOptions);
                google.maps.event.addListener(mainMap.googleMap, "click", function () { 
                    closeAllInfoWindows();
                });
                
                var dlvMapName = mainMap.params.dlvMapName;
                var dlvListName = mainMap.params.dlvListName;
                var feedURL = '/feed.rss?listname=' + dlvMapName;
                var queryString = getURLParameter("dlv_" + dlvListName)
                
                if(queryString != null)
                {
                    queryString = queryString.replace(/\(pageindex=[^\)]*\)/i, "").replace(/\(pagesize=[^\)]*\)/i, ""); //remove pageindex & pagesize
                    feedURL += "&dlv_" + dlvMapName + "=" + queryString;
                }
                
                $.ajax({
                    url: feedURL,
                    type: "GET",
                    dataType: "html",
                    success: function (data) {
                        $mapDataContainer = $($(mapContainer).find('.map-data')[0]);
                        $mapDataContainer.html(data);
                        
                        mainMap.markerSourceElements = $mapDataContainer.find('.gmap-marker');
                        AddMarkers(mainMap);
                        
                        if(mainMap.params.fitBounds === true && markers.length > 0)
                        {
                            SetBounds(mainMap);
                        }
                        var mcOptions = {
                            'zoom': 12, //M:1849 - was 13
                            'maxZoom': 15
                        };
                        mc = new MarkerClusterer(mainMap.googleMap, markers, mcOptions);
                    }
                });
            }
            
            //spiderfy
            //oms = new OverlappingMarkerSpiderfier(mainMap.googleMap);
            var iw = new google.maps.InfoWindow();
            oms.addListener('click', function(marker, event) {
              iw.setContent(marker.desc);
              iw.open(mainMap.googleMap, marker);
            });
            oms.addListener('spiderfy', function(markers) {
              iw.close();
            });
        }
 
        function CreateGoogleMap(mapContainer, data)
        {
            var centerPoint = GetCenterPoint(data);
            var mapOptions = {
                zoom : data.zoom,
                center : centerPoint,
                mapTypeId : google.maps.MapTypeId.ROADMAP
            };
            mapOptions.disableDefaultUI = data.hideControls === true;
            mapOptions.minZoom = (data.minZoom !== undefined) ? data.minZoom : 0;
            mapOptions.maxZoom = (data.maxZoom !== undefined) ? data.maxZoom : 21;
            return new google.maps.Map(mapContainer, mapOptions);
        }
 
        function GetCenterPoint(data)
        {
            if(data.centerPoint === undefined)
            {
                return new google.maps.LatLng(-37.818157, 144.960393);
            }
            var latLong = ConvertLatLongToFloat(data.centerPoint);
            if(latLong.length == 2)
            {
                return new google.maps.LatLng(latLong[0],latLong[1]);
            }
            return new google.maps.LatLng(-37.818157, 144.960393);
        }
 
        function AddMarkers(map)
        {
            $(map.markerSourceElements).each(function() {
                var latLongStr = $(this).find('.gmap-latlong').first().html();
                var address = $(this).find('.gmap-address').first().html();
                var info = $(this).find('.gmap-info').first().html();
                var latLong = ConvertLatLongToFloat(latLongStr);
                if($(this).find('.gmap-icon').length > 0)
                    var icon = ProcessIconLink($(this).find('.gmap-icon').first().html());
                else
                    var icon = null;
                var markerInfo = {
                    latLong : latLong,
                    address : address,
                    info : info,
                    maxWidth: 295
                };
                AddMarker(markerInfo, map, icon);
            });
        }
        
        function ProcessIconLink(link)
        {
            link = link.replace(/ &amp; /g, '_').replace(/ /g, '_').toLowerCase();
            
            var idx = link.lastIndexOf("/");
            var path = link.substr(0, idx + 1);
            var iconName = link.substr(idx + 1, link.length - idx - 1);
            
            if(iconName.indexOf("community") >= 0)
                iconName = "comm";
            else if(iconName.indexOf("event") >= 0)
                iconName = "upco";
            else if(iconName.indexOf("park") >= 0)
                iconName = "park";
            else if(iconName.indexOf("recreation") >= 0)
                iconName = "aqua";
            else if(iconName.indexOf("hall") >= 0)
                iconName = "halls";
            else if(iconName.indexOf("libra") >= 0)
                iconName = "libr";
            else if(iconName.indexOf("youth") >= 0 || iconName.indexOf("yfs") >= 0)
                iconName = "yout";
            else if(iconName.indexOf("maternal") >= 0 || iconName.indexOf("mch") >= 0)
                iconName = "mate";
            
            iconName = "flag_" + iconName + ".png"
            
            return path + iconName;
        }
 
        function ConvertLatLongToFloat(latLongStr)
        {
            var split = latLongStr.split(',');
            var latLong = [];
            if(split.length != 2)
            {
                return latLong;
            }
            latLong[0] = parseFloat(split[0]);
            latLong[1] = parseFloat(split[1]);
            return latLong;
        }
 
        function AddMarker(markerInfo, map, icon)
        {
            if(markerInfo.latLong.length == 2)
            {
                AddMarkerUsingLatLong(new google.maps.LatLng(
                    markerInfo.latLong[0], markerInfo.latLong[1]), markerInfo, map, icon);
            }
            else
            {
                AddMarkerUsingGeoCodedAddress(markerInfo, map, icon);
            }
        }
 
        function AddMarkerUsingLatLong(latLong, markerInfo, map, icon)
        {
            if(icon != null)            
                var marker = new google.maps.Marker({
                    position: latLong,
                    map: map.googleMap,
                    icon: icon
                });
            else
                var marker = new google.maps.Marker({
                    position: latLong,
                    map: map.googleMap
                });
            google.maps.event.addListener(marker, 'click', function(event)
            {
                map.infoWindow.setContent(markerInfo.info);
                map.infoWindow.open(map.googleMap, marker);
            });
            AddDomListenerToCenterOnAutoOpenedInfoWindow(markerInfo, map);
            //map.markers.push(marker);
            markers.push(marker);
            if($('.details-page.gmap').length) { // on the details page
                map.googleMap.setCenter(marker.getPosition()); //re-centre
            }
            oms.addMarker(marker);
        }
 
        function AddDomListenerToCenterOnAutoOpenedInfoWindow(markerInfo, map)
        {
            if(map.params.autoOpenInfoWindow === true && map.isMultiple === false)
            {
                google.maps.event.addDomListener(window, "load", function()
                {
                    map.infoWindow.setContent(markerInfo.info);
                    if(markers.length==1)
                    {
                        map.infoWindow.open(map.googleMap,markers[0]);
                    }
                });
            }
        }
 
        function AddMarkerUsingGeoCodedAddress(markerInfo, map, icon)
        {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { address : markerInfo.address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    var latLong = results[0].geometry.location;
                    AddMarkerUsingLatLong(latLong, markerInfo, map, icon);
                }
                else
                {
                    console.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
 
        function SetBounds(map)
        {
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < markers.length; i++)
            {
                var position = markers[i].position;
                bounds.extend(position);
            }
            map.googleMap.fitBounds(bounds);
        }
        
        //src: http://stackoverflow.com/questions/1403888/get-escaped-url-parameter
        function getURLParameter(name) {
            name = encodeURI(name);
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        }
 
    }(SEAMLESS.googleMaps = SEAMLESS.googleMaps || {}, jQuery));

// Place any jQuery/helper plugins in here.

$(document).ready(function () {
    SeamlessCity.Plugins.Pagination.init('.seamless-pagination');
    SeamlessCity.Plugins.HighlightText.init();
    SeamlessCity.Plugins.PredictiveSearch.init();
    SeamlessCity.Plugins.BackToTop.init('#back-to-top');
    SEAMLESS.googleMaps.initialise();
    
    $('a[data-sort="mapView"]').click(function() {
        if(!$('.gmap').hasClass('visuallyhidden')) { //if not active
            //setTimeout(function(){
            SEAMLESS.googleMaps.initialise();
            //}, 300); 
        }
    });
});